//
//  ValidationNameViewController.swift
//  Validation
//
//  Created by Alex on 02.03.2022.
//

import UIKit

final class ValidationNameViewController: UIViewController {
    
    override func loadView() {
        self.view = ValidationNameView()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        configureView()
        validateName()
    }
}


//MARK: - Private Methods

extension ValidationNameViewController {
    
    private func configureView() {
        view.backgroundColor = .systemBackground
    }
    
    private func validateName() {
        guard let nameView = view as? ValidationNameView else { return }
        nameView.nameCardView.textCallback = { text in
            let regExString = "[A-Za-zА-ЯЁа-яё-]{2,}+\\s{1}+[A-Za-zА-ЯЁа-яё-]{2,32}"
            let predicate = NSPredicate(format: "SELF MATCHES[c] %@", regExString)
            let isValid = predicate.evaluate(with: text)
            nameView.nameCardView.changeStateLabel(isTextValid: isValid)
        }
    }
}
